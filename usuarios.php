<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CGPDI Admin</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/datatables.min.css">

</head>
<body>
    <?php include_once "cabecalho.php"; ?>

    <main role="main" class="container">
        <h2>Usuários</h2>
        <hr>
        <ul class="nav nav-tabs" id="cabecalho.phpTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="cadastro-tab" data-toggle="tab" href="#cadastro" role="tab" aria-controls="cadastro" aria-selected="true">Cadastro</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="lista-tab" data-toggle="tab" href="#lista" role="tab" aria-controls="lista" aria-selected="false">Lista</a>
            </li>
        </ul>

        <div class="tab-content" id="cabecalho.phpTabConteudo">
            <div class="tab-pane fade show active" id="cadastro" role="tabpanel" aria-labelledby="cadastro-tab">
                <form method="post" id="form"  onreset="javascrit:$('#id').val('');">
                    <input type="hidden" name="id" id="id" />
                    <div class="form-group col-sm clearfix">
                        <button type="submit" class="btn btn-success float-right" id="btn-cadastrar">
                            <i class="fas fa-save"></i>
                        </button>

                        <button type="reset" class="btn btn-secondary float-right" id="btn-novo">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                    <div class="form-row">
                      <div class="form-group col-md-4">
                          <label for="nome">Nome</label>
                          <input type="text" class="form-control required" id="nome" name="nome" maxlength="40">
                      </div>

                      <div class="form-group col-md-4">
                          <label for="email">Email</label>
                          <input type="text" class="form-control required" id="email" name="email" maxlength="100">
                      </div>

                      <div class="form-group col-md-2">
                          <label for="senha">Senha de Acesso</label>
                          <input type="password" class="form-control required" id="senha" name="senha" maxlength="30">
                      </div>

                      <div class="form-group col-md-2">
                          <label for="conf-senha">Confirmação de Senha</label>
                          <input type="password" class="form-control required" id="conf-senha" name="conf-senha" maxlength="30">
                      </div>
                      
                      <div class="form-group col-md-4">
                          <label for="endereco">Endereço</label>
                          <input type="text" class="form-control" id="endereco" name="endereco" maxlength="40">
                      </div>
                      <div class="form-group col-md-1">
                          <label for="numero">Número</label>
                          <input type="number" class="form-control" id="numero" name="numero" >
                      </div>
                      <div class="form-group col-md-2">
                          <label for="bairro">Bairro</label>
                          <input type="text" class="form-control" id="bairro" name="bairro" maxlength="20">
                      </div>
                      <div class="form-group col-md-3">
                          <label for="cidade">Cidade</label>
                          <input type="text" class="form-control" id="cidade" name="cidade" maxlength="40">
                      </div>
                      <div class="form-group col-md-2">
                          <label for="uf">Estado</label>
                          <select class="form-control" id="uf" name="uf" >
                            <option value="">Selecione</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP" selected>São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                          </select>
                      </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-3">
                          <label for="complemento">Complemento</label>
                          <input type="text" class="form-control" id="complemento" name="complemento" maxlength="20">
                      </div>

                      <div class="form-group col-md-2">
                          <label for="cep">CEP</label>
                          <input type="text" class="form-control" id="cep" name="cep" maxlength="9">
                      </div>
                      
                      <div class="form-group col-md-2">
                          <label for="telefone">Telefone</label>
                          <input type="text" class="form-control" id="telefone" name="telefone" maxlength="14">
                      </div>

                      <div class="form-group col-md-1">
                          <label for="ramal">Ramal</label>
                          <input type="text" class="form-control" id="ramal" name="ramal" maxlength="4">
                      </div>

                      <div class="form-group col-md-2">
                          <label for="celular">Celular</label>
                          <input type="text" class="form-control" id="celular" name="celular" maxlength="15">
                      </div>

                      <div class="form-group col-md-2">
                          <label for="tipo">Tipo</label>
                          <select class="form-control required" id="tipo" name="tipo" >
                              <option value="A">Administrador</option>
                              <option value="N">Normal</option>
                              <option value="P" selected>Pesquisador</option>
                          </select>
                      </div>
                      </div>
                    <div class="form-row">
                      <div class="form-group col-md-2">
                          <label for="rg">RG</label>
                          <input type="text" class="form-control" id="rg" name="rg" maxlength="20">
                      </div>
                      <div class="form-group col-md-2">
                          <label for="cpf">CPF</label>
                          <input type="text" class="form-control" id="cpf" name="cpf" maxlength="20">
                      </div>

                      <div class="form-group col-md-4">
                          <label for="banco">Banco</label>
                          <input type="text" class="form-control" id="banco" name="banco" maxlength="35">
                      </div>

                      <div class="form-group col-md-2">
                          <label for="agencia">Agência</label>
                          <input type="text" class="form-control" id="agencia" name="agencia" maxlength="6">
                      </div>

                      <div class="form-group col-md-2">
                          <label for="conta">Conta</label>
                          <input type="text" class="form-control" id="conta" name="conta" maxlength="13">
                      </div>
                      
                      
                  </div>
                </form>
            </div>
            <div class="tab-pane fade" id="lista" role="tabpanel" aria-labelledby="lista-tab">
                <div class="alert alert-success hidden" role="alert" id="alerta-sucesso"></div>
                <div class="alert alert-danger hidden" role="alert" id="alerta-erro"></div>

                <hr>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="listaClientes">
                                <thead>
                                    <tr>
                                      <th>Id</th>
                                      <th>Nome</th>
                                      <th>E-mail</th>
                                      <th>Telefone</th>
                                      <th>Ramal</th>
                                      <th>Tipo</th>
                                      <th>Atualizar</th>
                                      <th>Remover</th>
                                    </tr>
                                </thead>
                                <tbody/>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include_once "modal/Modal.php";?>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/cgpdi.js"></script>
    <script src="js/modal.js"></script>

    <script>

        $(document).ready(function() {
            $("#cpf").mask("999.999.999-99");
            $("#rg").mask("99.999.999-9");
            $("#cep").mask("99999-999");
            $("#ramal").mask("9999");
            $("#telefone").mask("(99) 9999-9999");
            $("#celular").mask("(99) 99999-9999");
            $('table').DataTable({
                order: [[ 0, "desc" ]],
                ajax: {
                    url: "controller/usuario/lista.php",
                    dataSrc: ""
                },
                columns: [
                    { data: "id" },
                    { data: "nome" },
                    { data: "email" },
                    { data: "telefone" },
                    { data: "ramal" },
                    { data: "tipo" },
                    {"render": function (data, type, full, meta)
                        { return '<a class="btn btn-warning editar" id="' + full.id + '"><i class="far fa-edit"></i></a>'; }
                    },
                    {"render": function (data, type, full, meta)
                        { return '<a class="btn btn-danger remover" id="' + full.id + '"><i class="far fa-trash-alt"></i></a>'; }
                    }
                ],
                bAutoWidth: false,
                info: false,
                lengthChange: false,
                language: {
                    search: 'Busca:',
                    searchPlaceholder: "",
                    emptyTable: "Nenhum registro encontrado.",
                    paginate: {
                        "next": "Próxima",
                        "previous": "Anterior"
                    }
                }
            });

        });

        $('body').on('click', '.editar', function(){
            id = $(this).attr("id");
            $.post('controller/usuario/formulario.php',{id: id},function(data){
                usuario = JSON.parse(data)[0];
                $("#id").val(usuario.id);
                $("#nome").val(usuario.nome);
                $("#email").val(usuario.email);
                $("#senha").val(usuario.senha);
                $("#conf-senha").val(usuario.senha);
                $("#endereco").val(usuario.endereco);
                $("#numero").val(usuario.numero);
                $("#bairro").val(usuario.bairro);
                $("#cidade").val(usuario.cidade);
                $("#uf").val(usuario.uf);
                $("#complemento").val(usuario.complemento);
                $("#cep").val(usuario.cep);
                $("#telefone").val(usuario.telefone);
                $("#ramal").val(usuario.ramal);
                $("#celular").val(usuario.celular);
                $("#tipo").val(usuario.tipo);
                $("#rg").val(usuario.rg);
                $("#cpf").val(usuario.cpf);
                $("#banco").val(usuario.banco);
                $("#agencia").val(usuario.agencia);
                $("#conta").val(usuario.conta);
                $("#cadastro-tab").click();
            });
        });

        $('body').on('click', '.remover', function(){
            id = $(this).attr("id");
            modal({title: "Deseja realmente excluir esse usuário?", confirmTitle: "Sim", cancelTitle: "Não"}, function(){
                $.ajax({url: "controller/usuario/remover.php?id="+id, type: "post", dataType: "html", contentType: false, cache: false, processData: false, success: function (result) {
                    $("#alerta-sucesso").html(result).show();
                    $("#btn-novo").click();
                    $('table').DataTable().ajax.reload();
                },
                error: function (result) {
                    console.log(result);
                }});
            });

        });

        function limpaCampos(){
            $('input[type="text"], input[type="date"], input[type="hidden"]').val('');
        }

        $('form').submit(function (evt) {
            evt.preventDefault();
            if($("#senha").val() != $("#conf-senha").val()){
                modal({title: "Senha e confirmação de senha não conferem", cancel: false}, 
                    function(){
                        $("#senha,#conf-senha").val("").addClass("border-danger");
                    }
                );
            } else if(tools.validaCampos()){
                $.ajax({
                    url: "controller/usuario/salvar.php",
                    type: "post",
                    dataType: "html",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (result) {
                        $("#alerta-sucesso").html(result).show();
                         limpaCampos();
                         $("#btn-novo").click();
                    },
                    error: function (result) {
                        console.log(result);
                    }
                }).done(function(){
                    $('table').DataTable().ajax.reload();
                    $("#lista-tab").click();
                });
            }
        });
    </script>
</body>
</html>
