<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CGPDI Admin</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/datatables.min.css">

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container">
            <a class="navbar-brand" href="#">CGPDI Admin</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent"></div>
        </div>
    </nav>

    <main role="main" class="container">
        <h2>Documentos de Projetos</h2>

        <ul class="nav nav-tabs" id="cabecalho.phpTab" role="tablist"></ul>
        <div class="tab-content" id="documentosProjetos">
            <div class="tab-pane fade show active" id="cadastro" role="tabpanel" aria-labelledby="cadastro-tab">
                <div class="alert alert-danger hidden" role="alert" id="alerta-erro"></div>
                <div class="alert alert-success hidden" role="alert" id="alerta-sucesso"></div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="id_projeto"><strong>Pesquisador</strong></label>
                        <select class="form-control required" id="id_usuario" name="id_usuario"></select>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="id_projeto"><strong>Projeto</strong></label>
                        <select class="form-control required" id="projetos" name="projetos" ></select>
                    </div>
                </div>
                <br />
                <div class="form-row">
                    <div class="form-group col-md-5 c_princial_dir">        
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label for="nome"><strong>Início: </strong></label>
                                <label id="inicio"></label>
                                
                            </div>
                        
                            <div class="form-group col-md-5">
                                <label for="nome"><strong>Término:</strong></label>
                                <label id="termino"></label>
                                
                            </div>                           
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="nome"><strong>Descrição:</strong></label>
                                <div class="input-group">
                                    <label id="descricao"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-7"> 
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="nome"><strong>Arquivos</strong></label>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="listaClientes">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Data Cadastro</th>
                                            <th>Atualizar</th>
                                            <th>Doc.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="div-frame-office" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-full" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="titulo-arquivo">&nbsp;</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" id="btn-close-modal">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <iframe id="iframe-office"></iframe>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/cgpdi.js"></script>
    <script>
        var table;

        $('#id').mask('0#');    
        $('body .data-toggle').tooltip();

        var div_upload= $("#div-upload").html();
        
        setProjeto = function(id){
            $.post('controller/projeto/formulario.php',{id: id},function(proj){
                $("#id_projeto").val(proj[0].id);
                $("#inicio").text(proj[0].data_inicio);
                $("#termino").text(proj[0].data_fim);
                $("#descricao").text(proj[0].descricao);
                $("#area").val(proj[0].area);
            }, "json");       
        }
        
        $("#id_usuario").change(function() {
            $("#projetos").html("");
            $.getJSON('controller/projeto/lista_por_usuario.php?id_usuario='+$("#id_usuario").val(),function(projetos){
                for(i in projetos){
                    $("#projetos").append($('<option>', {value: projetos[i].id,text: projetos[i].nome}));
                }
                setProjeto($("#projetos").val());
                table.ajax.url("controller/publicacao/lista.php?id_projeto="+$("#projetos").val()).load();
            });

        });

        $("#projetos").change(function() {
            setProjeto($("#projetos").val());
            table.ajax.url("controller/publicacao/lista.php?id_projeto="+$("#projetos").val()).load();
        });

        $(document).ready(function() {
            $.getJSON('controller/usuario/responsaveis.php',function(responsaveis){
                for(i in responsaveis){
                    $("#id_usuario").append($('<option>', {value: responsaveis[i].id,text: responsaveis[i].nome}));
                }
                
                $.getJSON('controller/projeto/lista_por_usuario.php?id_usuario='+$("#id_usuario").val(),function(projetos){
                    for(i in projetos){
                        $("#projetos").append($('<option>', {value: projetos[i].id,text: projetos[i].nome}));
                    }
                    setProjeto($("#projetos").val());
                    table = $('table').DataTable({
                        order: [[ 0, "desc" ]],
                        ajax: {url: "controller/publicacao/lista.php?id_projeto="+$("#projetos").val(), dataSrc: ""},
                        columns: [
                            { data: "id" },
                            {"render": function (data, type, full, meta){ 
                                    niveis = full.caminho.split("/");
                                    return niveis[niveis.length-1];
                            }},
                            { data: "data_cadastro" },
                            {"render": function (data, type, full, meta){ 
                                niveis = full.caminho.split("/");
                                url = "upload/"+niveis[niveis.length-3]+"/"+niveis[niveis.length-2]+"/"+niveis[niveis.length-1];
                                
                                extensao = niveis[niveis.length-1].split('.').pop();
                                botao = "";

                                switch(extensao) {
                                    case 'docx':
                                    case 'doc':
                                        botao = "<button class='btn btn-primary data-toggle view-office' data-href='"+url+"' arquivo-nome='"+niveis[niveis.length-1]+"' title='Arquivo Word'><i class='fas fa-file-word'></i></button>";
                                        break;
                                    case 'pptx':
                                    case 'ppt':
                                    case 'pps':
                                        botao = "<button class='btn btn-warning data-toggle view-office' data-href='"+url+"' arquivo-nome='"+niveis[niveis.length-1]+"' title='Apresentação Power Point'><i class='fas fa-file-powerpoint'></i></button>";
                                        break;
                                    case 'xlsx':
                                    case 'xls':
                                        botao = "<button class='btn btn-success data-toggle view-office' data-href='"+url+"' arquivo-nome='"+niveis[niveis.length-1]+"' title='Planilha'><i class='fas fa-file-excel'></i></button>";
                                        break;
                                    default:
                                        botao = "<a class='btn btn-danger data-toggle' target='_blank' href='"+url+"' title='Arquivo PDF'><i class='far fa-file-pdf'></i></a>"
                                }

                                return botao;
                            }}
                        ],
                        createdRow: function( row, data, index ) {
                            if (data["visualizacao"] == 1) {
                                table.rows(row).remove().draw();
                            }
                        },
                        bAutoWidth: false,
                        info: false,
                        lengthChange: false,
                        language: {search: 'Busca:', searchPlaceholder: "", emptyTable: "Nenhum registro encontrado.", paginate: {"next": "Próxima", "previous": "Anterior"}}
                    });
                });
            });

            $('body [data-toggle="tooltip"]').tooltip();
        });

        $('body').on('click', '.view-office', function(){
            var alturaIframe = $('body').height();
            $("#titulo-arquivo").html('Visualizando o arquivo: ' + $(this).attr('arquivo-nome'));
            $("#iframe-office").attr("src", "https://view.officeapps.live.com/op/embed.aspx?src=http://cgpdi.org.br/cgpdi_admin/" + $(this).attr("data-href"));
            $("#div-frame-office").modal('show');
        });

        isOffice = function(arquivo){
            extensoes = [".doc", ".docx", ".ppt", ".pptx", ".ppsx", ".xls", ".xmlx", ".pps"];
            for(i in extensoes){
                if(arquivo.indexOf(extensoes[i]) !== -1){
                    return true
                }
            }
            return false;
        }

        listaArquivos = function(arquivos){
            paths = arquivos.split(";");
            
            $("#div-upload").hide();
            for(i in paths){
                nome = niveis[niveis.length-1];
                console.log(nome);
                $("#div-files").append("<a class='btn btn-warning data-toggle' target='_blank' href='"+url+"' title='"+nome+"'><i class='far fa-file-pdf'></i></a>");
            }
            $("#div-files").show();
        }
    </script>
</body>
</html>