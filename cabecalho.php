<?php include "controller/login/verificar.php"; ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    <a class="navbar-brand" href="#">CGPDI Admin</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <?php if($login_tipo != 'P'): ?>
        <li class="nav-item">
            <a class="nav-link" href="instituicoes.php">Instituições</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="licitacoes.php">Licitações</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="noticias.php">Notícias</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="projetos.php">Projetos</a>
          </li>
          <?php if($login_tipo == 'A'): ?>
            <li class="nav-item">
              <a class="nav-link" href="usuarios.php">Usuários</a>
            </li>
          <?php endif; ?>
        <?php endif; ?>
        <?php if($login_tipo == 'P' || $login_tipo == 'A'): ?>
          <li class="nav-item">
            <a class="nav-link" href="publicacoes.php">Publicações</a>
          </li>
        <?php endif;?>

      </ul>
      <ul class="navbar-nav my-2 my-lg-0">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user"></i>
                        Bem vindo, <?= $login_nome ?>
                    </a>
          <div class="dropdown-menu">
            <!-- <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                        <div class="dropdown-divider"></div> -->
            <a class="dropdown-item" href="controller/login/sair.php">Sair</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
