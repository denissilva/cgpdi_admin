<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CGPDI Admin</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/datatables.min.css">

</head>
<body>
    <?php include_once "cabecalho.php"; ?>

    <main role="main" class="container">
        <h2>Projetos</h2>
        <hr>
        <ul class="nav nav-tabs" id="cabecalho.phpTab" role="tablist">
            <?php if($login_tipo == 'A'): ?>
            <li class="nav-item">
                <a class="nav-link active" id="cadastro-tab" data-toggle="tab" href="#cadastro" role="tab" aria-controls="cadastro" aria-selected="true">Cadastro</a>
            </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link" id="lista-tab" data-toggle="tab" href="#lista" role="tab" aria-controls="lista" aria-selected="false">Lista</a>
            </li>
        </ul>

        <div class="tab-content" id="cabecalho.phpTabConteudo">
            <?php if($login_tipo == 'A'): ?>
            <div class="tab-pane fade show active" id="cadastro" role="tabpanel" aria-labelledby="cadastro-tab">
                <form method="post" id="form" onreset="javascrit:$('#id').val('');$('#area').prop('readonly', false);">
                    <input type="hidden" name="id" id="id" />
                    <div class="form-group col-sm clearfix">
                        <button type="submit" class="btn btn-success float-right" id="btn-cadastrar">
                            <i class="fas fa-save"></i>
                        </button>

                        <button type="reset" class="btn btn-secondary float-right" id="btn-novo">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control required" id="nome" name="nome" maxlength="150">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="sigla">Sigla</label>
                            <input type="text" class="form-control" id="sigla" name="sigla" maxlength="40">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="area">Área</label>
                            <input type="text" class="form-control" id="area" name="area" maxlength="20" >
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="data_inicio">Data de Inicio</label>
                            <input type="date" class="form-control required" id="data_inicio" name="data_inicio" maxlength="100">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="data_fim">Data de Fim</label>
                            <input type="date" class="form-control required" id="data_fim" name="data_fim" maxlength="100">
                        </div>
                        <div class="form-group col-md-5">
                            <label for="link">Link</label>
                            <input type="text" class="form-control" id="link" name="link" maxlength="100">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="id_usuario">Pesquisador</label>
                            <select class="form-control required" id="id_usuario" name="id_usuario" ></select>
                        </div>
                    </div>
                    <div class="form-row">
                    <div class="form-group col-md-4">
                            <label for="data_fim">Instituições</label>
                            <select class="form-control required" id="instituicao" name="instituicao[]" multiple="multiple" size="6"></select>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="descricao">Descrição</label>
                            <textarea class="form-control" id="descricao" name="descricao" rows="6" maxlength="600"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <?php endif; ?>
            <div class="tab-pane fade" id="lista" role="tabpanel" aria-labelledby="lista-tab">
                <div class="alert alert-success hidden" role="alert" id="alerta-sucesso"></div>
                <div class="alert alert-danger hidden" role="alert" id="alerta-erro"></div>

                <hr>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="listaClientes">
                                <thead>
                                    <tr>
                                      <th>id</th>
                                      <th>Nome</th>
                                      <th>Sigla</th>
                                      <?php
                                        if($_SESSION["login_tipo"] == 'A'){
                                            echo "<th>Atualizar</th><th>Remover</th>";
                                        }
                                      ?>
                                    </tr>
                                </thead>
                                <tbody/>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/cgpdi.js"></script>

    <script>
        <?php if($login_tipo == 'N'): ?>
            $('#lista-tab').click();
        <?php endif; ?>

        $(document).ready(function() {
            $.getJSON('controller/usuario/responsaveis.php',function(usuarios){
                for(i in usuarios){
                    $("#id_usuario").append($('<option>', {value: usuarios[i].id,text: usuarios[i].nome}));
                }
            });
            $.getJSON('controller/instituicao/lista.php',function(instituicoes){
                for(i in instituicoes){
                    $("#instituicao").append($('<option>', {value: instituicoes[i].id,text: instituicoes[i].nome}));
                }
            });

            $('table').DataTable({
                order: [[ 0, "desc" ]],
                ajax: {
                    url: "controller/projeto/lista.php",
                    dataSrc: ""
                },
                columns: [
                    { data: "id" },
                    { data: "nome" },
                    { data: "sigla" }
                    <?php
                        if($_SESSION["login_tipo"] == 'A'){
                            echo ",{\"render\": function (data, type, full, meta){ return '<a class=\"btn btn-warning editar\" id=\"' + full.id + '\"><i class=\"far fa-edit\"></i></a>'; }}";
                            echo ",{\"render\": function (data, type, full, meta){ return '<a class=\"btn btn-danger remover\" id=\"' + full.id + '\"><i class=\"far fa-trash-alt\"></i></a>';}}";
                        }
                    ?>
                ],
                bAutoWidth: false,
                info: false,
                lengthChange: false,
                language: {
                    search: 'Busca:',
                    searchPlaceholder: "",
                    emptyTable: "Nenhum registro encontrado.",
                    paginate: {
                        "next": "Próxima",
                        "previous": "Anterior"
                    }
                }
            });

        });

        $('body').on('click', '.editar', function(){
            id = $(this).attr("id");
            $.post('controller/projeto/formulario.php',{id: id},function(data){
                projeto = JSON.parse(data)[0];
                $("#id").val(projeto.id);
                $("#sigla").val(projeto.sigla);
                $("#nome").val(projeto.nome);
                $("#descricao").val(projeto.descricao);
                $("#data_inicio").val(projeto.data_inicio);
                $("#data_fim").val(projeto.data_fim);
                $("#instituicao").val(projeto.instituicao);
                $("#link").val(projeto.link);
                console.log("usuario id", projeto.id_usuario);
                $("#id_usuario").val(projeto.id_usuario);
                $("#area").val(projeto.area);
                
                instituicoes = projeto.instituicao.split(";");

                for(i in instituicoes)
                    $("#instituicao option[value="+instituicoes[i]+"]").prop("selected", true);

                $("#area").prop( "readonly", projeto.area != undefined && projeto.area != "");
                
                $("#cadastro-tab").click();

            });
        });

        $('body').on('click', '.remover', function(){

            if(confirm("Deseja realmente excluir essa projeto?")){
                id = $(this).attr("id");
                console.log(id);
                $.ajax({url: "controller/projeto/remover.php?id="+id, type: "post", dataType: "html", contentType: false, cache: false, processData: false, success: function (result) {
                    $("#alerta-sucesso").html(result).show();
                    $("#btn-novo").click();
                    $('table').DataTable().ajax.reload();
                },
                error: function (result) {
                    console.log(result);
                }});
            }

        });

        function limpaCampos(){
            $("#area").prop( "readonly", false);
            $('input[type="text"], input[type="date"], input[type="hidden"]').val('');
        }

        $('form').submit(function (evt) {
            evt.preventDefault();
            if(tools.validaCampos()){
                $.ajax({
                    url: "controller/projeto/salvar.php",
                    type: "post",
                    dataType: "html",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (result) {
                        $("#alerta-sucesso").html(result).show();
                        limpaCampos();
                        $("#btn-novo").click();
                    },
                    error: function (result) {
                        console.log(result);
                    }
                }).done(function(){
                    $('table').DataTable().ajax.reload();
                    $("#lista-tab").click();
                });
            }
        });

        <?php
            if($_SESSION["login_tipo"] == 'A'){	
                //JS PURO
                echo "var inicio = document.getElementById('data_inicio');var fim = document.getElementById('data_fim');";
                echo "fim.addEventListener('change', function() {if (fim.value)inicio.max = fim.value;}, false);";
                echo "inicio.addEventListener('change', function() {if (inicio.value)fim.min = inicio.value;}, false);";
            }
        ?>
    </script>
</body>
</html>