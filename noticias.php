<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CGPDI Admin</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/datatables.min.css">
</head>
<body>
    <?php include_once "cabecalho.php"; ?>

    <?php include_once "modal/Mensagem.php"; ?>

    <main role="main" class="container">
        <h2>Notícias</h2>
        <hr>
        <ul class="nav nav-tabs" id="cabecalho.phpTab" role="tablist">
            <?php if($login_tipo == 'A'): ?>
            <li class="nav-item">
                <a class="nav-link active" id="cadastro-tab" data-toggle="tab" href="#cadastro" role="tab" aria-controls="cadastro" aria-selected="true">Cadastro</a>
            </li>
            <?php endif; ?>
            <li class="nav-item">
                <a class="nav-link" id="lista-tab" data-toggle="tab" href="#lista" role="tab" aria-controls="lista" aria-selected="false">Lista</a>
            </li>
        </ul>

        <div class="tab-content" id="cabecalho.phpTabConteudo">
            <?php if($login_tipo == 'A'): ?>
            <div class="tab-pane fade show active" id="cadastro" role="tabpanel" aria-labelledby="cadastro-tab">
                <form method="post" enctype="multipart/form-data" onreset="javascrit:$('#id').val('');">
                    <input type="hidden" name="id" id="id" />
                    <div class="form-group col-sm clearfix">
                        <div class="btn-group float-right" role="group">
                            <button type="submit" class="btn btn-success " id="btn-cadastrar">
                                <i class="fas fa-save"></i>
                            </button>
                            <button type="reset" class="btn btn-secondary" id="btn-novo">
                                <i class="fas fa-plus"></i>
                            </button>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="titulo">Título</label>
                            <input type="text" class="form-control required" id="titulo" name="titulo" maxlength="75">
                        </div>
                        <div class="form-group col-md-2">
                            <label for="data_inicio">Data Início</label>
                            <input type="date" class="form-control required" id="data_inicio" name="data_inicio" >
                        </div>
                        <div class="form-group col-md-2">
                            <label for="data_termino">Data Término</label>
                            <input type="date" class="form-control required" id="data_termino" name="data_termino" >
                        </div>
                        <div class="form-group col-md-2">
                            <label for="situacao">Situação</label>
                            <select class="form-control required" id="situacao" name="situacao" >
                                <option value="1" selected>Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="link">Link</label>
                            <input type="text" class="form-control required" id="link" name="link" maxlength="150">
                        </div>
                        <div class="form-group col-md-6" id="div-upload">
                            <label for="upload">Upload Imagem</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input required" id="file" name="file">
                                    <label class="custom-file-label" for="file" id="fileLabel">Escolha uma imagem...</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="descricao">Descrição da Notícia</label>
                            <textarea class="form-control" id="descricao" name="descricao" rows="2" maxlength="250"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <?php endif; ?>
            <div class="tab-pane fade" id="lista" role="tabpanel" aria-labelledby="lista-tab">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="listaClientes">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Título</th>
                                        <th>Situação</th>
                                        <?php
                                            if($_SESSION["login_tipo"] == 'A'){
                                                echo "<th>Atualizar</th><th>Remover</th>";
                                            }
                                        ?>
                                    </tr>
                                </thead>
                                <tbody/>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include_once "modal/Modal.php";?>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/cgpdi.js"></script>
    <script src="js/modal.js"></script>

    <script>
        <?php if($login_tipo == 'N'): ?>
            $('#lista-tab').click();
        <?php endif; ?>

        var div_upload= $("#div-upload").html();

        $("#btn-novo").click(function(){
            $("#id").val("");
            $("#fileLabel").html('Escolha uma imagem...');
            $("#div-upload").html(div_upload).show();
        });

        $(document).ready(function() {
            $('table').DataTable({
                order: [[ 0, "desc" ]],
                ajax: {
                    url: "controller/noticia/lista.php",
                    dataSrc: ""
                },
                columns: [
                    { data: "id" },
                    { data: "titulo" },
                    {"render": function (data, type, full, meta)
                        { return  full.situacao ? "Ativo" : "Inativo"; }
                    }
                    <?php
                        if($_SESSION["login_tipo"] == 'A'){	
                            echo ",{\"render\": function (data, type, full, meta){ return '<a class=\"btn btn-warning editar\" id=\"' + full.id + '\"><i class=\"far fa-edit\"></i></a>'; }}";
                            echo ",{\"render\": function (data, type, full, meta){ return '<a class=\"btn btn-danger remover\" id=\"' + full.id + '\"><i class=\"far fa-trash-alt\"></i></a>'; }}";
                        }
                    ?>
                ],
                bAutoWidth: false,
                info: false,
                lengthChange: false,
                language: {
                    search: 'Busca:',
                    searchPlaceholder: "",
                    emptyTable: "Nenhum registro encontrado.",
                    paginate: {
                        "next": "Próxima",
                        "previous": "Anterior"
                    }
                }
            });

        });

        $('body').on('click', '.editar', function(){
            id = $(this).attr("id");
            $.post('controller/noticia/formulario.php',{id: id},function(data){
                noticia = JSON.parse(data)[0];
                $("#id").val(noticia.id);
                $("#titulo").val(noticia.titulo);
                $("#situacao").val(noticia.situacao);
                $("#link").val(noticia.link);
                $("#data_inicio").val(noticia.data_inicio);
                $("#data_termino").val(noticia.data_termino);
                $("#descricao").val(noticia.descricao);
                $("#cadastro-tab").click();
                $("#div-upload").html("").hide();
            });
        });

        $('body').on('click', '.remover', function(){
            modal({title: "Deseja realmente excluir essa notícia?", confirmTitle: "Sim", cancelTitle: "Não"}, function(){
                id = $(this).attr("id");
                console.log(id);
                $.ajax({url: "controller/noticia/remover.php?id="+id, type: "post", dataType: "html", contentType: false, cache: false, processData: false, success: function (result) {
                    mostraMensagem(result);
                    $("#btn-novo").click();
                    $('table').DataTable().ajax.reload();
                },
                error: function (result) {
                    console.log(result);
                }});
            });
        });

        function limpaCampos(){
            $('input[type="text"], input[type="date"]').val('');
            $("#fileLabel").html('Escolha uma imagem...');
        }

        $('form').submit(function (evt) {
            evt.preventDefault();

            if(tools.validaCampos()){
                $.ajax({
                    url: "controller/noticia/salvar.php",
                    type: "post",
                    dataType: "html",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (result) {
                        mostraMensagem(result);
                        $("#btn-novo").click();
                    },
                    error: function (result) {
                        console.log(result);
                    }
                }).done(function(){
                    $('table').DataTable().ajax.reload();
                    $("#lista-tab").click();
                });
            }
        });

        $("#file").change(function() {
            var arquivo = this.files[0];
            var extensaoArquivo = arquivo.type;
            var extensoes= ["image/jpeg","image/png","image/jpg"];

            if(!((extensaoArquivo==extensoes[0]) || (extensaoArquivo==extensoes[1]) || (extensaoArquivo==extensoes[2]))){
                modal({title: "Escolha um arquivo valido (.jpg, .jpeg, .png)", cancel: false}, function(){
                    $("#fileLabel").html('Escolha uma imagem...');
                });
                return false;
            }else{
                $("#fileLabel").html(arquivo.name);
            }
        });

        <?php
            if($_SESSION["login_tipo"] == 'A'){	
                //JS PURO
                echo "var inicio = document.getElementById('data_inicio');var fim = document.getElementById('data_termino');";
                echo "fim.addEventListener('change', function() {if (fim.value)inicio.max = fim.value;}, false);";
                echo "inicio.addEventListener('change', function() {if (inicio.value)fim.min = inicio.value;}, false);";
            }
        ?>
    </script>
</body>
</html>
