<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CGPDI Admin</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/datatables.min.css">

</head>
<body>
    <?php include_once "cabecalho.php";?>

    <main role="main" class="container">
        <h2>Licitações</h2>
        <ul class="nav nav-tabs" id="cabecalho.phpTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="cadastro-tab" data-toggle="tab" href="#cadastro" role="tab" aria-controls="cadastro" aria-selected="true">Cadastro</a>
            </li>
                
            <li class="nav-item">
                <a class="nav-link" id="lista-tab" data-toggle="tab" href="#lista" role="tab" aria-controls="lista" aria-selected="false">Lista</a>
            </li>
        </ul>
        
        <div class="tab-content" id="cabecalho.phpTabConteudo">
            <div class="tab-pane fade show active" id="cadastro" role="tabpanel" aria-labelledby="cadastro-tab">
                <form method="post" enctype="multipart/form-data" onreset="javascrit:$('#id').val('');">
                    <input type="hidden" class="form-control" id="id" name="id">
                    <div class="form-group col-sm clearfix">
                        <button type="submit" class="btn btn-success float-right" id="btn-cadastrar">
                            <i class="fas fa-save"></i>
                        </button>
                        
                        <button class="btn btn-secondary float-right" id="btn-novo">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="situacao">Tipo de Processo</label>
                            <select class="form-control required" id="tipo" name="tipo" >
                                <option value="" class="hidden">Selecione</div>
                                <option value="LI">Licitação</option>        
                                <option value="PE">Pregão Eletrônico</option>
                                <option value="PP">Pregão Presencial</option>
                                <option value="TP">Tomada de Preço</option>
                                <option value="OU">Outros</option>
                                
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="codigo">Nº Processo</label>
                            <input type="text" class="form-control required" id="codigo" name="codigo" minlength="6" maxlength="6">
                        </div>
                        <div class="form-group col-md-8">
                            <label for="titulo">Título</label>
                            <input type="text" class="form-control required" id="titulo" name="titulo" maxlength="75">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label for="abertura">Abertura</label>
                            <input type="date" class="form-control required" id="abertura" name="abertura" >
                        </div>
                        <div class="form-group col-md-2">
                            <label for="situacao">Situação</label>
                            <select class="form-control required" id="situacao" name="situacao" >
                                <option value="1">Ativo</option>
                                <option value="0">Inativo</option>
                            </select>
                        </div>
                        <div class="form-group col-md-8" id="div-upload">
                            <label for="upload">Upload Imagem</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input required" id="files" name="files[]" multiple="multiple">
                                    <label class="custom-file-label" for="files" id="fileLabel">Escolha os arquivos...</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-8 hidden" id="div-files"></div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="lista" role="tabpanel" aria-labelledby="lista-tab">
                <div class="alert alert-danger hidden" role="alert" id="alerta-erro"></div>
                <div class="alert alert-success hidden" role="alert" id="alerta-sucesso"></div>
                <hr />
                <div class="row">
                    <div class="col-sm">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="listaClientes">
                                <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Tipo</th>
                                        <th>Título</th>
                                        <th>Situação</th>
                                        <th>Abertura</th>
                                        <th>Editar</th>
                                        <th>Remover</th>
                                    </tr>
                                </thead>
                                <tbody/>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include_once "modal/Modal.php";?>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/cgpdi.js"></script>
    <script src="js/modal.js"></script>

    <script>
        $('#codigo').mask('0#');    
        $('body .data-toggle').tooltip();

        var div_upload= $("#div-upload").html();

        $("#btn-novo").click(function(){
            $('input[type="text"], input[type="date"], input[type="hidden"], input[type="file"]').val('');
            //$("#fileLabel").html('');
            $("#div-upload").html(div_upload).show();
            $("#div-files").html("").hide();
        });
        
        $(document).ready(function() {
            $('table').DataTable({
                order: [[ 0, "desc" ]],
                ajax: {
                    url: "controller/licitacao/lista.php",
                    dataSrc: ""
                },
                columns: [
                    { data: "codigo" },
                    {"render": function (data, type, full, meta)
                        {
                            if(full.tipo == "TP")        return "Tomada de Preço";
                            else if(full.tipo == "PE")   return "Pregão Eletrônico";           
                            else if(full.tipo == "PP")   return "Pregão Presencial";
                            else if(full.tipo == "LI")   return "Licitação";
                            else                         return "Outros";
                        }
                    },
                    { data: "titulo" },
                    { data: "abertura" },
                    
                    {"render": function (data, type, full, meta)
                        { return  full.situacao == 1 ? "Ativo" : "Inativo"; }
                    },
                    {"render": function (data, type, full, meta)
                        { return '<a class="btn btn-warning editar" id="' + full.id + '"><i class="far fa-edit"></i></a>'; }
                    },
                    {"render": function (data, type, full, meta)
                        { return '<a class="btn btn-danger remover" id="' + full.id + '"><i class="far fa-trash-alt"></i></a>'; }
                    }
                ],
                bAutoWidth: false,
                info: false,
                lengthChange: false,
                language: {
                    search: 'Busca:',
                    searchPlaceholder: "",
                    emptyTable: "Nenhum registro encontrado.",
                    paginate: {
                        "next": "Próxima",
                        "previous": "Anterior"
                    }
                }
            });

            $('body [data-toggle="tooltip"]').tooltip();
        });

        $('body').on('change', '#codigo', function(){
            codigo = $("#codigo").val();
            tipo = $("#tipo").val();
            $.get('controller/licitacao/checkCodigoTipo.php',{codigo: codigo,tipo: tipo},function(data){
                if(JSON.parse(data).length > 0){
                    $("#alerta-erro").html("Código "+codigo+" já utilizado para esse tipo de processo.").show();
                    $("#codigo").addClass("border-danger");
                    setTimeout(function(){ $("#alerta-erro").fadeOut()}, 5000);
                }
                else{
                    $("#codigo").removeClass("border-danger");
                }
            }); 
        });

        $('body').on('click', '.editar', function(){
            id = $(this).attr("id");
            $.get('controller/licitacao/formulario.php',{id: id},function(data){
                licitacao = JSON.parse(data)[0];
                $("#id").val(licitacao.id);
                $("#codigo").val(licitacao.codigo);
                $("#tipo").val(licitacao.tipo);
                $("#titulo").val(licitacao.titulo);
                $("#situacao").val(licitacao.situacao);
                $("#abertura").val(licitacao.abertura);
                listaArquivos(licitacao.arquivos);
                $("#cadastro-tab").click();
                $("#div-upload").html("").hide();
            });            
        });

        $('body').on('click', '.remover', function(){
            id = $(this).attr("id");
            modal({title: "Deseja realmente excluir essa Licitação?", confirmTitle: "Sim", cancelTitle: "Não"}, function(){
                $.ajax({url: "controller/licitacao/remover.php?id="+id, type: "post", dataType: "html", contentType: false, cache: false, processData: false, success: function (result) {
                    $("#alerta-sucesso").html(result).show();
                    $("#btn-novo").click();
                    $('table').DataTable().ajax.reload();
                    $('.border-danger').removeClass("border-danger");
                },
                error: function (result) {
                    console.log(result);
                }}); 
            });       
        });

        $('form').submit(function (evt) {
            evt.preventDefault();
            
            if(tools.validaCampos()){
                $.ajax({
                    url: "controller/licitacao/salvar.php",
                    type: "post",
                    dataType: "html",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (result) {
                        $("#alerta-sucesso").html(result).show();
                        $("#btn-novo").click();
                        $('.border-danger').removeClass("border-danger");
                    },
                    error: function (result) {
                        console.log(result);
                    }
                }).done(function(){
                    $('table').DataTable().ajax.reload();
                    $("#lista-tab").click();
                });
            }
        });

        listaArquivos = function(arquivos){
            paths = arquivos.split(";");
            
            $("#div-upload").hide();
            $("#div-files").html("<label>Arquivo</label><br />");
            for(i in paths){
                niveis = paths[i].split("/");
                url = "upload/"+niveis[niveis.length-3]+"/"+niveis[niveis.length-2]+"/"+niveis[niveis.length-1];
                nome = niveis[niveis.length-1];
                $("#div-files").append("<a class='btn btn-warning data-toggle d-inline-block' target='_blank' href='"+url+"' title='"+nome+"'><i class='far fa-file-pdf'></i></button>");
            }
            $("#div-files").show();
        }

        $("#files").change(function() {
            $("#fileLabel").html("");            
            
            valido = true;
            for (i = 0; i < this.files.length; i++){
                fileType = this.files[i].type;
                
                if(fileType != "application/pdf"){
                    valido = false;
                    break;
                }else{
                    $("#fileLabel").append(this.files[i].name+";");
                }
            }
            
            if(!valido){
                modal({title: "Escolha um arquivo valido (.pdf)", cancel: false}, function(){
                    $("#fileLabel").html('Selecione os arquivos PDF...');
                });
            }

            return valido;
        });
    </script>
</body>
</html>