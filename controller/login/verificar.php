<?php
    session_start();
    if (isset($_SESSION["login_id"])) {
        $login_id    = $_SESSION["login_id"];
        $login_email = $_SESSION["login_email"];
        $login_nome  = $_SESSION["login_nome"];
        $login_tipo  = $_SESSION["login_tipo"];

        $pagina = array_reverse(explode('/', $_SERVER['REQUEST_URI']))[0];

        if ($login_tipo == 'N') {
            //PAGINAS QUE O USUARIO NORMAL TEM ACESSO
            $liberada = ["index.php", "instituicoes.php", "noticias.php", "projetos.php"];
            if (!in_array($pagina, $liberada)) {
                header("location: ./controller/login/sair.php");
            }
        }
        else if($login_tipo == 'P'){
             $liberada = ["publicacoes.php"];
             if (!in_array($pagina, $liberada)) {
                 header("location: ./controller/login/sair.php");
             }
        }

    } else {
        header("location: ./controller/login/sair.php");
    }