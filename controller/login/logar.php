<?php
    require_once ('../../classes/Login.php');
    require_once ('../../dao/LoginDao.php');


    $login = new Login();
    $dao   = new LoginDao();

    $login->setEmail($_POST['txtEmail']);
    $login->setSenha($_POST['txtSenha']);

    try {
        if ($dao->logar($login) == '[]') {
            header("location: ../../login.php");
        } else {
            $usuario = json_decode($dao->logar($login));
            session_start();
            $_SESSION["login_id"]    = $usuario[0]->id;
            $_SESSION["login_email"] = $usuario[0]->email;
            $_SESSION["login_nome"]  = $usuario[0]->nome;
            $_SESSION["login_tipo"]  = $usuario[0]->tipo;

            if($usuario[0]->tipo == 'P')
                header("location: ../../publicacoes.php");
            else
                header("location: ../../projetos.php");
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
