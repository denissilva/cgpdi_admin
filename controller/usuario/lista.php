<?php  
    require_once ('../../dao/UsuarioDao.php');

    $dao     = new UsuarioDao();

    try {
        $result = $dao->getUsuarios();
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    