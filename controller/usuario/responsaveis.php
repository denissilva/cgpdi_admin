<?php  
    require_once ('../../dao/UsuarioDao.php');

    $dao     = new UsuarioDao();

    try {
        session_start();
        if ($_SESSION["login_tipo"] == 'P') {
            $result = $dao->getUsuario($_SESSION["login_id"]);    
        }
        else{
            $result = $dao->getUsuariosPorTipo('P   ');
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    