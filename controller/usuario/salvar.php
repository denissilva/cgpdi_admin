<?php
    require_once ('../../classes/Usuario.php');
    require_once ('../../dao/UsuarioDao.php');


    $usuario = new Usuario();
    $dao     = new UsuarioDao();

    $usuario->setNome($_POST['nome']);
    $usuario->setEmail($_POST['email']);
    $usuario->setSenha($_POST['senha']);
    $usuario->setEndereco($_POST['endereco']);
    $usuario->setNumero($_POST['numero']);
    $usuario->setBairro($_POST['bairro']);
    $usuario->setCidade($_POST['cidade']);
    $usuario->setUf($_POST['uf']);
    $usuario->setComplemento($_POST['complemento']);
    $usuario->setCep($_POST['cep']);
    $usuario->setTelefone($_POST['telefone']);
    $usuario->setRamal($_POST['ramal']);
    $usuario->setCelular($_POST['celular']);
    $usuario->setTipo($_POST['tipo']);
    $usuario->setRg($_POST['rg']);
    $usuario->setCpf($_POST['cpf']);
    $usuario->setBanco($_POST['banco']);
    $usuario->setAgencia($_POST['agencia']);
    $usuario->setConta($_POST['conta']);
    
    try {
        if (empty($_POST['id'])) {
            $dao->inserir($usuario);
        }else{
            $usuario->setId($_POST['id']);
            $dao->alterar($usuario);
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
