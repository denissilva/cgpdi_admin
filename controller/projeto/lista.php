<?php
    require_once ('../../dao/ProjetoDao.php');

    $dao     = new ProjetoDao();

    try {
        if (empty($_GET['campo'])) {
            $result = $dao->getProjetos();
        }else{
            $result = $dao->getProjetosOrderBy($_GET['campo']);
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
