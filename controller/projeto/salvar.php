<?php
    require_once ('../../classes/Projeto.php');
    require_once ('../../dao/ProjetoDao.php');


    $projeto = new Projeto();
    $dao     = new ProjetoDao();

    $projeto->setSigla($_POST["sigla"]);
    $projeto->setNome($_POST["nome"]);
    $projeto->setDescricao($_POST["descricao"]);
    $projeto->setData_inicio($_POST["data_inicio"]);
    $projeto->setData_fim($_POST["data_fim"]);
    $projeto->setInstituicao(join(';', $_POST["instituicao"]));
    $projeto->setLink($_POST["link"]);
    $projeto->setId_usuario($_POST["id_usuario"]);

    if(empty($_POST["area"])){
        session_start();
        session_regenerate_id();
        $projeto->setArea(session_id());    
    }else{
        $projeto->setArea($_POST["area"]);
    }

    try {
        if (empty($_POST['id'])) {
            $dao->inserir($projeto);
        }else{
            $projeto->setId($_POST['id']);
            $dao->alterar($projeto);
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
