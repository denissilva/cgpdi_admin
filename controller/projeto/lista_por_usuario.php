<?php
    require_once ('../../dao/ProjetoDao.php');

    $dao     = new ProjetoDao();

    try {
        session_start();
        $dao->getProjetosPorUsuario($_GET['id_usuario']);
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
