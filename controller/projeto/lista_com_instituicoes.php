<?php
    require_once ('../../dao/ProjetoDao.php');

    $dao     = new ProjetoDao();

    try {
        $result = $dao->getProjetosComInstituicoes($_GET['campo']);
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
