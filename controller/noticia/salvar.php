<?php  
    require_once ('../../classes/Noticia.php');
    require_once ('../../dao/NoticiaDao.php');
    

    $noticia = new noticia();
    $dao     = new NoticiaDao();
    
    $noticia->setTitulo($_POST['titulo']);
    $noticia->setSituacao($_POST['situacao']);
    $noticia->setLink($_POST['link']);
    $noticia->setDataInicio($_POST['data_inicio']);
    $noticia->setDataTermino($_POST['data_termino']);
    $noticia->setDescricao($_POST['descricao']);


    try {
        if (empty($_POST['id'])) {
            $noticia->setImagem($_FILES['file']);
            $dao->inserir($noticia);
        }else{
            $noticia->setId($_POST['id']);
            $dao->alterar($noticia);
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
    



	
