<?php  
    require_once ('../../dao/NoticiaDao.php');

    $dao     = new NoticiaDao();

    try {
        $result = $dao->getNoticias();
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    