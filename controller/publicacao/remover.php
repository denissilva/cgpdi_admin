<?php  
    require_once ('../../dao/PublicacaoDao.php');

    $dao     = new PublicacaoDao();

    try {
        session_start();
        if (isset($_SESSION["login_tipo"]) && $_SESSION["login_tipo"] != 'P') {
            $dao->remover($_GET['id']);
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    