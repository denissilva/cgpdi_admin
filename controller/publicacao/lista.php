<?php  
    require_once ('../../dao/PublicacaoDao.php');

    $dao     = new PublicacaoDao();

    try {
        $result = $dao->getPublicacoes($_GET['id_projeto']);
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    