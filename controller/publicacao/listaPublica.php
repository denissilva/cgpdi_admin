<?php  
    require_once ('../../dao/PublicacaoDao.php');

    $dao     = new PublicacaoDao();

    try {
        $result = $dao->getPublicacoesPublica($_GET['id_projeto']);
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    