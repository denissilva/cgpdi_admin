<?php  
    require_once ('../../classes/Instituicao.php');
    require_once ('../../dao/InstituicaoDao.php');
    
    $instituicao = new Instituicao();
    $dao     = new InstituicaoDao();
    
    $instituicao->setNome($_POST['nome']);
    
    try {
        if (empty($_POST['id'])) {
            $dao->inserir($instituicao, $_FILES['files']);
        }else{
            $instituicao->setId($_POST['id']);
            $dao->alterar($instituicao);
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
    



	
