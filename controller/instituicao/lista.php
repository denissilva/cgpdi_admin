<?php  
    require_once ('../../dao/InstituicaoDao.php');

    $dao     = new InstituicaoDao();

    try {
        $result = $dao->getInstituicoes();
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    