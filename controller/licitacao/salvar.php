<?php  
    require_once ('../../classes/Licitacao.php');
    require_once ('../../dao/LicitacaoDao.php');
    
    $licitacao = new licitacao();
    $dao     = new LicitacaoDao();
    
    $licitacao->setCodigo($_POST['codigo']);
    $licitacao->setTipo($_POST['tipo']);
    $licitacao->setTitulo($_POST['titulo']);
    $licitacao->setSituacao($_POST['situacao']);
    //$licitacao->setObs($_POST['obs']);
    $licitacao->setAbertura($_POST['abertura']);

    try {
        if (empty($_POST['id'])) {
            $dao->inserir($licitacao, $_FILES['files']);
        }else{
            $licitacao->setId($_POST['id']);
            $dao->alterar($licitacao);
        }
    } catch (Exception $ex) {
        return $ex->getMessage();
    }
    



	
