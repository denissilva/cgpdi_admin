<?php  
    require_once ('../../dao/LicitacaoDao.php');

    $dao = new LicitacaoDao();

    try {
        $dao->getLicitacaoPorCodigo($_GET['codigo'], $_GET['tipo']);
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    