<?php  
    require_once ('../../dao/LicitacaoDao.php');

    $dao     = new LicitacaoDao();

    try {
        $result = $dao->getLicitacoes();
    } catch (Exception $ex) {
        return $ex->getMessage();
    }    