var callbackModal;
function modal(obj, callback, err){
	$("#texto-modal").html(obj.title ? obj.title : "Deseja realmente executar essa ação?");
	$("#confirmaModal").html(obj.confirmTitle ? obj.confirmTitle : "OK");
	
	if(obj.cancel == false){
		$("#cancelaModal").hide();
	}else{
		$("#cancelaModal").show();
		$("#cancelaModal").html(obj.cancelTitle ? obj.cancelTitle : "CANCELAR");
	}
	
	$("#modal").modal("show");
	
	callbackModal = callback ? callback : function(){};
}

function fechaModal(retorno){
	$("#modal").modal("hide");
	return retorno;
}

$("#confirmaModal").click(function(){
	callbackModal();
	fechaModal(true);
});

$('#cancelaModal').on('click', function(e){
	e.preventDefault();
	fechaModal(false);
});

$('.cd-popup').on('click', function(event){
	if($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup') ) {
		event.preventDefault();
		fechaModal(false);
	}
});

$(document).keyup(function(event){
	if(event.which=='27'){
		fechaModal(false);
    }
});