
var tools = {
	validaCampos:(function(){
		var retorno = true;
        $( ".required" ).each(function( index ) {
            if($(this).val() == ""){
                if($(this).attr("type") == "file"){
                    $(this).closest('.input-group').addClass("border-danger");
                }else{
                    $(this).addClass("border-danger");
                }
                retorno = false;
            }else{
                $(this).removeClass("border-danger");
                $(this).closest('.input-group').removeClass("border-danger");
            }
        });
        return retorno;
	})
}

// $('body').on('change', '#alerta-erro', function(){
//     console.log("entrou");
//     $("#alerta-erro").delay(1000).fadeOut();
// });