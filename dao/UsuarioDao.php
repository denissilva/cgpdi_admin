<?php

    require_once ('../../classes/Usuario.php');
    require_once ('../../classes/Database.php');

    class UsuarioDao {
        private $conexao;
        public function __construct(){
            $db = new Database();
            $this->conexao = $db->getConection();
        }

        public function inserir(Usuario $usuario) {
            try {
                $nome     = $usuario->getNome();
                $email    = $usuario->getEmail();
                $senha    = $usuario->getSenha();
                $endereco = $usuario->getEndereco();
                $numero   = $usuario->getNumero();
                $bairro   = $usuario->getBairro();
                $cidade   = $usuario->getCidade();
                $uf       = $usuario->getUf();
                $complemento=$usuario->getComplemento();
                $cep      = $usuario->getCep();
                $telefone = $usuario->getTelefone();
                $ramal    = $usuario->getRamal();
                $celular  = $usuario->getCelular();
                $tipo     = $usuario->getTipo();
                $rg       = $usuario->getRg();
                $cpf      = $usuario->getCpf();
                $banco    = $usuario->getBanco();
                $agencia  = $usuario->getAgencia();
                $conta    = $usuario->getConta();

            
                $query = "INSERT INTO  usuarios_novo  (nome, email, senha, endereco,numero, bairro, cidade, uf, complemento, cep, telefone, ramal, celular, tipo, rg, cpf, banco, agencia, conta) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'ssssissssssisssssss', $nome, $email, $senha, $endereco, $numero, $bairro, $cidade, $uf, $complemento, $cep, $telefone, $ramal, $celular, $tipo, $rg, $cpf, $banco, $agencia, $conta);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                echo 'Usuário cadastrado com sucesso.';
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function alterar(Usuario $usuario) {
          $id       = $usuario->getId();
          $nome     = $usuario->getNome();
          $email    = $usuario->getEmail();
          $senha    = $usuario->getSenha();
          $endereco = $usuario->getEndereco();
          $numero   = $usuario->getNumero();
          $bairro   = $usuario->getBairro();
          $cidade   = $usuario->getCidade();
          $uf       = $usuario->getUf();
          $complemento=$usuario->getComplemento();
          $cep      = $usuario->getCep();
          $telefone = $usuario->getTelefone();
          $ramal    = $usuario->getRamal();
          $celular  = $usuario->getCelular();
          $tipo     = $usuario->getTipo();
          $rg       = $usuario->getRg();
          $cpf      = $usuario->getCpf();
          $banco    = $usuario->getBanco();
          $agencia  = $usuario->getAgencia();
          $conta    = $usuario->getConta();

            try {
                $query = "UPDATE  usuarios_novo  SET nome=?, email=?, senha=?, endereco=?, numero=?, bairro=?, cidade=?, uf=?, complemento=?, cep=?, telefone=?, ramal=?, celular=?, tipo=?, rg=?, cpf=?, banco=?, agencia=?, conta=? WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'ssssissssssisssssssi', $nome, $email, $senha, $endereco, $numero, $bairro, $cidade, $uf, $complemento, $cep, $telefone, $ramal, $celular, $tipo, $rg, $cpf, $banco, $agencia, $conta, $id);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                echo "Usuário alterada com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function remover($id) {
            try {
                $query = "DELETE FROM  usuarios_novo  WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'i', $id);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                echo "Usuário removido com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getUsuario($id) {
            try{
                $sql = 'SELECT * FROM  usuarios_novo  WHERE id='.$id;
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getUsuarios() {
            try{
                $sql = 'SELECT * FROM usuarios_novo';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }
                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getUsuariosPorTipo($tipo) {
            try{
                $sql = 'SELECT * FROM usuarios_novo WHERE tipo="'.$tipo.'"';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }
                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

    }
?>
