<?php

    require_once ('../../classes/Publicacao.php');
    require_once ('../../classes/Database.php');
    require_once ('../../util/Upload.php');

    
    class PublicacaoDao {
        private $conexao;

        //Caminho físico
        private $pasta;

        //Caminho lógico
        private $pasta_logica;
        
        public function __construct(){
            $db = new Database();
            $this->conexao = $db->getConection();
            
            $this->pasta = dirname(__DIR__)."/upload/PROJ/";
            $this->pasta_logica = "/cgpdi_admin/upload/PROJ/";
        }
        

        public function inserir($id_projeto, $area, $arqs, $visualizacao) {
            $this->pasta .= $area."/";
            $this->pasta_logica .= $area."/";

            if (!file_exists($this->pasta)) {
                mkdir($this->pasta, 0777, true);
            }
            
            $file_count = count($arqs['name']);
            for ($i=0; $i<$file_count; $i++) {
                try{
                    if (move_uploaded_file ($arqs['tmp_name'][$i], $this->pasta.$arqs['name'][$i]) ) {
                        $caminho = $this->pasta_logica.$arqs['name'][$i];
                        try {
                            $query = "INSERT INTO publicacoes_novo (id_projeto, caminho, visualizacao) VALUES(?,?,?)";
                            $stmt = mysqli_prepare($this->conexao, $query);
                            mysqli_stmt_bind_param($stmt, 'iss', $id_projeto, $caminho, $visualizacao);
                            mysqli_stmt_execute($stmt); 
                            mysqli_stmt_close($stmt);
                            echo "Arquivo ".$arqs['name'][$i]." enviado com sucesso.<br />";
                        } catch (Exception $ex) {
                            echo json_encode($arqs);
                            //echo $ex->getMessage();
                        } 
                    }
                }catch(Exception $ex){
                    echo $ex->getMessage()+" <br/>";
    
                }
            }                  
        }

        public function remover($id) {
            try{
                $sql = 'SELECT * FROM publicacoes_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                while($row = $result->fetch_assoc()) {
                    echo($row[$this->pasta.'caminho']);
                    unlink($row[$this->pasta.'caminho']);
                }
            
                $query = "DELETE FROM publicacoes_novo WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'i', $id);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Arquivo removido com sucesso.";    
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
        }

        public function getPublicacao($id) {
            try{
                $sql = 'SELECT * FROM publicacoes_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }

        public function getPublicacoes($id_projeto) {
            try{
                $sql = 'SELECT * FROM publicacoes_novo WHERE id_projeto='.$id_projeto;
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }

        public function getPublicacoesPublica($id_projeto) {
            try{
                $sql = 'SELECT * FROM publicacoes_novo WHERE id_projeto='.$id_projeto.' AND visualizacao=1';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }
    }
?>