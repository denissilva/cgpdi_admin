<?php

    require_once ('../../classes/Projeto.php');
    require_once ('../../classes/Database.php');

    class ProjetoDao {
        private $conexao;
        public function __construct(){
            $db = new Database();
            $this->conexao = $db->getConection();
        }

        public function inserir(Projeto $projeto) {
            $sigla        = $projeto->getSigla();
            $nome         = $projeto->getNome();
            $descricao    = $projeto->getDescricao();
            $data_inicio  = $projeto->getData_inicio();
            $data_fim     = $projeto->getData_fim();
            $instituicao  = $projeto->getInstituicao();
            $link         = $projeto->getLink();
            $id_usuario  = $projeto->getId_usuario();
            $area  = $projeto->getArea();
            // id, sigla, nome, descricao, data_inicio, data_fim, instituicao

            try {
                $query = "INSERT INTO projetos_novo (sigla, nome, descricao, data_inicio, data_fim, instituicao, link, id_usuario, area) VALUES(?,?,?,?,?,?,?,?,?)";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'sssssssis', $sigla, $nome, $descricao, $data_inicio, $data_fim, $instituicao, $link, $id_usuario, $area);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                echo "Projeto cadastrado com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function alterar(Projeto $projeto) {
          $id           = $projeto->getId();
          $sigla        = $projeto->getSigla();
          $nome         = $projeto->getNome();
          $descricao    = $projeto->getDescricao();
          $data_inicio  = $projeto->getData_inicio();
          $data_fim     = $projeto->getData_fim();
          $instituicao  = $projeto->getInstituicao();
          $link         = $projeto->getLink();
          $id_usuario  = $projeto->getId_usuario();
          $area         = $projeto->getArea();

            try {
                $query = "UPDATE projetos_novo SET sigla=?, nome=?, descricao=?, data_inicio=?, data_fim=?, instituicao=?, link=?, id_usuario=?, area=? WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'sssssssisi', $sigla, $nome, $descricao, $data_inicio, $data_fim, $instituicao, $link, $id_usuario, $area, $id);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                echo "Projeto alterado com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function remover($id) {
            try {
                $query = "DELETE FROM projetos_novo WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'i', $id);
                mysqli_stmt_execute($stmt);
                mysqli_stmt_close($stmt);
                echo "Projeto removido com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getProjeto($id) {
            try{
                $sql = 'SELECT * FROM projetos_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getProjetos() {
            try{
                $sql = 'SELECT * FROM projetos_novo ORDER BY nome';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getProjetosComInstituicoes($campo) {
            try{
                $sql = 'SELECT p.*, i.caminho FROM projetos_novo p, instituicoes_novo i WHERE p.instituicao = i.id ORDER BY '.$campo . ' DESC';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getProjetosPorUsuario($id_usuario) {
            try{
                $sql = 'SELECT * FROM projetos_novo WHERE id_usuario='.$id_usuario.' ORDER BY nome';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

        public function getProjetosOrderBy($campo) {
            try{
                $sql = 'SELECT * FROM projetos_novo ORDER BY '.$campo . ' DESC';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }

    }
?>
