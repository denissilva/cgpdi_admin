<?php

    require_once ('../../classes/Instituicao.php');
    require_once ('../../classes/Database.php');
    require_once ('../../util/Upload.php');

    
    class InstituicaoDao {
        private $conexao;
        
        public function __construct(){
            $db = new Database();
            $this->conexao = $db->getConection();
            
        }

        public function inserir(Instituicao $instituicao, $arqs) {
            $nome     = $instituicao->getNome();
            
            $upload = new Upload("INSTITUICOES");
            $caminho = $upload->fromFiles($arqs);
			                        
            try {
				$query = "INSERT INTO instituicoes_novo (nome, caminho) VALUES(?,?)";
				$stmt = mysqli_prepare($this->conexao, $query);
				mysqli_stmt_bind_param($stmt, 'ss', $nome, $caminho);
				mysqli_stmt_execute($stmt); 
				mysqli_stmt_close($stmt);
				echo "Instituição cadastrada com sucesso";
			} catch (Exception $ex) {
				echo $ex->getMessage();
			} 
            
        }

        public function alterar(Instituicao $instituicao) {
            $nome = $instituicao->getNome();
            $id = $instituicao->getId();
			
            try {
                $query = "UPDATE instituicoes_novo SET nome=? WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'si', $nome, $id);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Instituição alterada com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
        }

        public function remover($id) {
            try{
                $sql = 'SELECT * FROM instituicoes_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                while($row = $result->fetch_assoc()) {
                    $upload = new Upload("INSTITUICOES");
                    $upload->removeFile($row["caminho"]);
                }
            
                $query = "DELETE FROM instituicoes_novo WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'i', $id);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Instituição removida com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
        }

        public function getInstituicao($id) {
            try{
                $sql = 'SELECT * FROM instituicoes_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }

        public function getInstituicoes() {
            try{
                $sql = 'SELECT * FROM instituicoes_novo ORDER BY nome';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }
    }
?>