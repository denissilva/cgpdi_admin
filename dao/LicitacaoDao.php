<?php

    require_once ('../../classes/Licitacao.php');
    require_once ('../../classes/Database.php');
    require_once ('../../util/Upload.php');

    
    class LicitacaoDao {
        private $conexao;
        
        public function __construct(){
            $db = new Database();
            $this->conexao = $db->getConection();
            
        }

        public function inserir(Licitacao $licitacao, $arqs) {
            
            $codigo     = $licitacao->getCodigo();
            $tipo       = $licitacao->getTipo();
            $titulo     = $licitacao->getTitulo();
            $situacao   = $licitacao->getSituacao();
            $abertura   = $licitacao->getAbertura();

            $upload = new Upload($tipo."/".$codigo);
            $arquivos = $upload->fromFiles($arqs);
			                        
            try {
				$query = "INSERT INTO licitacoes_novo (codigo, tipo, titulo, situacao, abertura, arquivos) VALUES(?,?,?,?,?,?)";
				$stmt = mysqli_prepare($this->conexao, $query);
				mysqli_stmt_bind_param($stmt, 'sssiss', $codigo, $tipo, $titulo, $situacao, $abertura, $arquivos);
				mysqli_stmt_execute($stmt); 
				mysqli_stmt_close($stmt);
				echo "Licitação cadastrada com sucesso";
			} catch (Exception $ex) {
				echo $ex->getMessage();
			} 
            
        }

        public function alterar(Licitacao $licitacao) {
            $codigo = $licitacao->getCodigo();
            $tipo = $licitacao->getTipo();
            $titulo = $licitacao->getTitulo();
            $situacao = $licitacao->getSituacao();
            $abertura = $licitacao->getAbertura();
            $id = $licitacao->getId();
			
            try {
                $query = "UPDATE licitacoes_novo SET codigo=?, tipo=?, titulo=?, situacao=?, abertura=? WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'sssisi', $codigo, $tipo, $titulo, $situacao, $abertura, $id);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Licitação alterada com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
        }

        public function remover($id) {
            try{
                $sql = 'SELECT * FROM licitacoes_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                while($row = $result->fetch_assoc()) {
                    $upload = new Upload($row["tipo"]."/".$row["codigo"]);
                    $upload->remove();
                }
            
                $query = "DELETE FROM licitacoes_novo WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'i', $id);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Licitação removida com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
        }

        public function getLicitacao($id) {
            try{
                $sql = 'SELECT * FROM licitacoes_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }

        public function getLicitacaoPorCodigo($codigo, $tipo) {
            try{
                $sql = "SELECT * FROM licitacoes_novo WHERE codigo=".$codigo." AND tipo='".$tipo."'";
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }

        public function getLicitacoes() {
            try{
                $sql = 'SELECT * FROM licitacoes_novo';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }
    }
?>