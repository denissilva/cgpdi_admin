<?php

    require_once ('../../classes/Noticia.php');
    require_once ('../../classes/Database.php');
    require_once ('../../util/Upload.php');

    class NoticiaDao {
        private $conexao;
        public function __construct(){
            $db = new Database();
            $this->conexao = $db->getConection();
        }

        public function inserir(Noticia $noticia) {
            $titulo         = $noticia->getTitulo();
            $situacao       = $noticia->getSituacao();
            $link           = $noticia->getLink();
            $data_inicio    = $noticia->getDataInicio();
            $data_termino   = $noticia->getDataTermino();
            $descricao      = $noticia->getDescricao();
            
            $upload = new Upload("NOTICIA");
            $imagem = $upload->fromFile($noticia->getImagem());
            try {
                $query = "INSERT INTO noticias_novo (titulo, situacao, link, imagem, data_inicio, data_termino, descricao) VALUES(?,?,?,?,?,?,?)";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'sisssss', $titulo, $situacao, $link, $imagem, $data_inicio, $data_termino, $descricao);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Notícia cadastrada com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }           
        }

        public function alterar(Noticia $noticia) {
            $id             = $noticia->getId();
            $titulo         = $noticia->getTitulo();
            $situacao       = $noticia->getSituacao();
            $link           = $noticia->getLink();
            $data_inicio    = $noticia->getDataInicio();
            $data_termino   = $noticia->getDataTermino();
            $descricao      = $noticia->getDescricao();

            try {
                $query = "UPDATE noticias_novo SET titulo=?, situacao=?, link=?, data_inicio=?, data_termino=?, descricao=? WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'sissssi', $titulo, $situacao, $link, $data_inicio, $data_termino, $descricao, $id);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Notícia alterada com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
        }

        public function remover($id) {
            try {
                $sql = 'SELECT * FROM noticias_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                if($row = $result->fetch_assoc()) {                    
                    try{unlink($row["imagem"]);}catch(Exception $ex){} 
                }
                
                $query = "DELETE FROM noticias_novo WHERE id=?";
                $stmt = mysqli_prepare($this->conexao, $query);
                mysqli_stmt_bind_param($stmt, 'i', $id);
                mysqli_stmt_execute($stmt); 
                mysqli_stmt_close($stmt);
                echo "Notícia removida com sucesso!";
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }  
        }

        public function getNoticia($id) {
            try{
                $sql = 'SELECT * FROM noticias_novo WHERE id='.$id;
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }
                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }

        public function getNoticias() {
            try{
                $sql = 'SELECT * FROM noticias_novo';
                $result = $this->conexao->query($sql);

                $rows = array();
                if($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                        $rows[] = $row;
                    }
                }

                echo json_encode($rows);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            } 
        }
    }
?>