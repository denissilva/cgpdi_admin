<?php
  class Usuario{
    private $id;
    private $nome;
    private $email;
    private $senha;
    private $endereco;
    private $numero;
    private $bairro;
    private $cidade;
    private $uf;
    private $complemento;
    private $cep;
    private $telefone;
    private $ramal;
    private $celular;
    private $tipo;
    private $rg;
    private $cpf;
    private $banco;
    private $agencia;
    private $conta;
    
    public function getId(){
      return $this->id;
    }
  
    public function setId($id){
      $this->id = $id;
    }
  
    public function getNome(){
      return $this->nome;
    }
  
    public function setNome($nome){
      $this->nome = $nome;
    }
  
    public function getEmail(){
      return $this->email;
    }
  
    public function setEmail($email){
      $this->email = $email;
    }
  
    public function getSenha(){
      return $this->senha;
    }
  
    public function setSenha($senha){
      $this->senha = $senha;
    }
  
    public function getEndereco(){
      return $this->endereco;
    }
  
    public function setEndereco($endereco){
      $this->endereco = $endereco;
    }
  
    public function getNumero(){
      return $this->numero;
    }
  
    public function setNumero($numero){
      $this->numero = $numero;
    }
  
    public function getBairro(){
      return $this->bairro;
    }
  
    public function setBairro($bairro){
      $this->bairro = $bairro;
    }
  
    public function getCidade(){
      return $this->cidade;
    }
  
    public function setCidade($cidade){
      $this->cidade = $cidade;
    }
  
    public function getUf(){
      return $this->uf;
    }
  
    public function setUf($uf){
      $this->uf = $uf;
    }
  
    public function getComplemento(){
      return $this->complemento;
    }
  
    public function setComplemento($complemento){
      $this->complemento = $complemento;
    }
  
    public function getCep(){
      return $this->cep;
    }
  
    public function setCep($cep){
      $this->cep = $cep;
    }
  
    public function getTelefone(){
      return $this->telefone;
    }
  
    public function setTelefone($telefone){
      $this->telefone = $telefone;
    }
  
    public function getRamal(){
      return $this->ramal;
    }
  
    public function setRamal($ramal){
      $this->ramal = $ramal;
    }
  
    public function getCelular(){
      return $this->celular;
    }
  
    public function setCelular($celular){
      $this->celular = $celular;
    }
  
    public function getTipo(){
      return $this->tipo;
    }
  
    public function setTipo($tipo){
      $this->tipo = $tipo;
    }
  
    public function getRg(){
      return $this->rg;
    }
  
    public function setRg($rg){
      $this->rg = $rg;
    }
  
    public function getCpf(){
      return $this->cpf;
    }
  
    public function setCpf($cpf){
      $this->cpf = $cpf;
    }
  
    public function getBanco(){
      return $this->banco;
    }
  
    public function setBanco($banco){
      $this->banco = $banco;
    }
  
    public function getAgencia(){
      return $this->agencia;
    }
  
    public function setAgencia($agencia){
      $this->agencia = $agencia;
    }
  
    public function getConta(){
      return $this->conta;
    }
  
    public function setConta($conta){
      $this->conta = $conta;
    }

  }
?>
