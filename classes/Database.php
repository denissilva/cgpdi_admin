<?php
    setlocale(LC_ALL,'pt_BR.UTF8');
    mb_internal_encoding('UTF8'); 
    mb_regex_encoding('UTF8');

    class Database {
        
        private $host = "localhost";
        private $username = "root";
        private $password = "";
        private $database = "cgpdi_admin";

        //private $host = "localhost";
        //private $username = "aplbaorg";
        //private $password = "cg0506pdi";
        //private $database = "aplbaorg_intranet";

        private $conexao = null; 

        public function __construct(){          
            $this->conect();
        }

        public function getConection(){
            return $this->conexao;
        }

        private function conect(){
            $this->conexao = mysqli_connect(
                $this->host, 
                $this->username, 
                $this->password, 
                $this->database
            );
        }
    }

?>