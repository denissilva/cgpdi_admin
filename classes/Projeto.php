<?php
  class Projeto{
    private $id;
    private $sigla;
    private $nome;
    private $descricao;
    private $data_inicio;
    private $data_fim;
    private $id_usuario;
    private $instituicao;
    private $link;
    private $area;

    public function getId(){
      return $this->id;
    }

    public function setId($id){
      $this->id = $id;
    }

    public function getSigla(){
      return $this->sigla;
    }

    public function setSigla($sigla){
      $this->sigla = $sigla;
    }

    public function getNome(){
      return $this->nome;
    }

    public function setNome($nome){
      $this->nome = $nome;
    }

    public function getDescricao(){
      return $this->descricao;
    }

    public function setDescricao($descricao){
      $this->descricao = $descricao;
    }

    public function getData_inicio(){
      return $this->data_inicio;
    }

    public function setData_inicio($data_inicio){
      $this->data_inicio = $data_inicio;
    }

    public function getData_fim(){
      return $this->data_fim;
    }

    public function setData_fim($data_fim){
      $this->data_fim = $data_fim;
    }

    public function getId_usuario(){
      return $this->id_usuario;
    }

    public function setId_usuario($id_usuario){
      $this->id_usuario = $id_usuario;
    }

    public function getInstituicao(){
      return $this->instituicao;
    }

    public function setInstituicao($instituicao){
      $this->instituicao = $instituicao;
    }

    public function getLink(){
      return $this->link;
    }

    public function setLink($link){
      $this->link = $link;
    }

    public function getArea(){
      return $this->area;
    }

    public function setArea($area){
      $this->area = $area;
    }
  }
?>
