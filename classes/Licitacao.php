<?php
    class Licitacao{
        private $id;
        private $codigo;
        private $tipo;
        private $titulo;
        private $situacao;
        private $arquivos;
        private $obs;
        private $abertura;
        
        function getId(){
            return $this->id;
        }

        function setId($i){
            $this->id = $i;
        }

        function getCodigo(){
            return $this->codigo;
        }

        function setCodigo($c){
            $this->codigo = $c;
        }

        function getTipo(){
            return $this->tipo;
        }

        function setTipo($t){
            $this->tipo = $t;
        }
        
        function getTitulo(){
            return $this->titulo;
        }

        function setTitulo($t){
            $this->titulo = $t;
        }

        function getSituacao(){
            return $this->situacao;
        }

        function setSituacao($s){
            $this->situacao = $s;
        }

        function getArquivos(){
            return $this->arquivos;
        }

        function setArquivos($arqs){
            $this->arquivos = $arqs;
        }

        function getAbertura(){
            return $this->abertura;
        }

        function setAbertura($d){
            $this->abertura = $d;
        }
		
		function getObs(){
            return $this->obs;
        }

        function setObs($l){
            $this->obs = $l;
        }
    }
?>