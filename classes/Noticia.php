<?php
    class Noticia{
        private $id;
        private $titulo;
        private $situacao;
        private $imagem;
        private $link;
        private $data_inicio;
        private $data_termino;
        private $descricao;

        function getId(){
            return $this->id;
        }

        function setId($i){
            $this->id = $i;
        }
        
        function getTitulo(){
            return $this->titulo;
        }

        function setTitulo($t){
            $this->titulo = $t;
        }

        function getSituacao(){
            return $this->situacao;
        }

        function setSituacao($s){
            $this->situacao = $s;
        }

        function getImagem(){
            return $this->imagem;
        }

        function setImagem($i){
            $this->imagem = $i;
        }

        function getLink(){
            return $this->link;
        }

        function setLink($l){
            $this->link = $l;
        }

        function getDataInicio(){
            return $this->data_inicio;
        }

        function setDataInicio($d){
            $this->data_inicio = $d;
        }

        function getDataTermino(){
            return $this->data_termino;
        }

        function setDataTermino($d){
            $this->data_termino = $d;
        }

        function getDescricao(){
            return $this->descricao;
        }

        function setDescricao($d){
            $this->descricao = $d;
        }
    }
?>