<?php
    class Instituicao{
        private $id;
        private $nome;
        private $arquivo;
        
        function getId(){
            return $this->id;
        }

        function setId($id){
            $this->id = $id;
        }

        function getNome(){
            return $this->nome;
        }

        function setNome($nome){
            $this->nome = $nome;
        }

        function getArquivo(){
            return $this->arquivo;
        }

        function setArquivo($arquivo){
            $this->arquivo = $arquivo;
        }
    }
?>