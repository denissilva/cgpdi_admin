<?php
  class Publicacao{
    private $id;
    private $nome;
    private $caminho;
    private $id_projeto;
    private $data_cadastro;

    public function getId(){
      return $this->id;
    }

    public function setId($id){
      $this->id = $id;
    }

    public function getNome(){
      return $this->nome;
    }

    public function setNome($nome){
      $this->nome = $nome;
    }

    public function getCaminho(){
      return $this->caminho;
    }

    public function setCaminho($caminho){
      $this->caminho = $caminho;
    }

    public function getId_projeto(){
      return $this->id_projeto;
    }

    public function setId_projeto($id_projeto){
      $this->id_projeto = $id_projeto;
    }

    public function getData_cadastro(){
      return $this->data_cadastro;
    }

    public function setData_cadastro($data_cadastro){
      $this->data_cadastro = $data_cadastro;
    }
  }
    
?>
