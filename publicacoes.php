<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CGPDI Admin</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/datatables.min.css">
</head>
<body>
    <?php include_once "cabecalho.php";?>

    <main role="main" class="container">
        <h2>Documentos de Projetos</h2>

        <ul class="nav nav-tabs" id="cabecalho.phpTab" role="tablist"></ul>
        <div class="tab-content" id="documentosProjetos">
            <div class="tab-pane fade show active" id="cadastro" role="tabpanel" aria-labelledby="cadastro-tab">
                <div class="alert alert-danger hidden" role="alert" id="alerta-erro"></div>
                <div class="alert alert-success hidden" role="alert" id="alerta-sucesso"></div>

                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label for="id_projeto"><strong>Pesquisador</strong></label>
                        <select class="form-control required" id="id_usuario" name="id_usuario"></select>
                    </div>
                    <div class="form-group col-md-8">
                        <label for="id_projeto"><strong>Projeto</strong></label>
                        <select class="form-control required" id="projetos" name="projetos" ></select>
                    </div>
                </div>
                <br />
                <div class="form-row">
                    <div class="form-group col-md-5 c_princial_dir">        
                        <div class="form-row">
                            <div class="form-group col-md-7">
                                <label for="nome"><strong>Início: </strong></label>
                                <label id="inicio"></label>
                                
                            </div>
                        
                            <div class="form-group col-md-5">
                                <label for="nome"><strong>Término:</strong></label>
                                <label id="termino"></label>
                                
                            </div>                           
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="nome"><strong>Descrição:</strong></label>
                                <div class="input-group">
                                    <label id="descricao"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-7"> 
                        <div class="form-row">
                            <div class="form-group col-md-11">
                                <label for="nome"><strong>Arquivos</strong></label>
                            </div>
                            <div class="form-group col-md-1">
                                <?php 
                                    if ($_SESSION["login_tipo"] != 'P') {
                                        echo '<button class=\'btn btn-success\' id=\'" + full.id + "\' data-toggle=\'modal\' data-target=\'#div-modal-add-pub\'><i class=\'fas fa-plus\'></i></button>';
                                    } 
                                ?>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped" id="listaClientes">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>Data Cadastro</th>
                                            <th>Atualizar</th>
                                            <th>Doc.</th>
                                            <?php if ($_SESSION["login_tipo"] != 'P') {echo '<th>Remover</th>';} ?>
                                        </tr>
                                    </thead>
                                    <tbody/>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="modal fade" id="div-modal-add-pub" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLongTitle">Adicionar documentos ao Projeto</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" id="btn-close-modal">&times;</span>
                                </button>
                            </div>
                            <form method="post" enctype="multipart/form-data">
                                <div class="modal-body">
                                    <input type="hidden" class="form-control" id="id_projeto" name="id_projeto">
                                    <input type="hidden" class="form-control" id="area" name="area">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input required" id="files" name="files[]" multiple="multiple">
                                                <label class="custom-file-label" for="files" id="fileLabel">Escolha os arquivos...</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="visualizacao">Visualização</label>
                                        <select class="form-control" id="visualizacao" name="visualizacao">
                                            <option value="0">Privado</option>
                                            <option value="1">Público</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" id="salvar" class="btn btn-primary float-right">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="div-frame-office" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-full" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="titulo-arquivo">&nbsp;</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" id="btn-close-modal">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <iframe id="iframe-office"></iframe>
                            </div>
                            <!-- <div class="modal-footer">
                                <button type="submit" id="salvar" class="btn btn-primary float-right">Enviar</button>
                            </div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include_once "modal/Modal.php";?>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/modal.js"></script>
    <script src="js/cgpdi.js"></script>
    <script>
        <?php echo 'var tipo_usuario = "'.$_SESSION['login_tipo'].'";' ?>;
        var table;

        $("#id_usuario").prop("disabled", tipo_usuario == 'P');
        $('#id').mask('0#');    
        $('body .data-toggle').tooltip();

        var div_upload= $("#div-upload").html();

        $("#btn-novo").click(function(){
            $('input[type="text"], input[type="date"], input[type="hidden"], input[type="file"]').val('');
            //$("#fileLabel").html('');
            $("#div-upload").html(div_upload).show();
            $("#div-files").html("").hide();
        });
        
        setProjeto = function(id){
            $.post('controller/projeto/formulario.php',{id: id},function(proj){
                $("#id_projeto").val(proj[0].id);
                $("#inicio").text(proj[0].data_inicio);
                $("#termino").text(proj[0].data_fim);
                $("#descricao").text(proj[0].descricao);
                $("#area").val(proj[0].area);
            }, "json");       
        }
        
        $("#id_usuario").change(function() {
            $("#projetos").html("");
            $.getJSON('controller/projeto/lista_por_usuario.php?id_usuario='+$("#id_usuario").val(),function(projetos){
                for(i in projetos){
                    $("#projetos").append($('<option>', {value: projetos[i].id,text: projetos[i].nome}));
                }
                setProjeto($("#projetos").val());
                table.ajax.url("controller/publicacao/lista.php?id_projeto="+$("#projetos").val()).load();
            });

        });
        $("#projetos").change(function() {
            setProjeto($("#projetos").val());
            table.ajax.url("controller/publicacao/lista.php?id_projeto="+$("#projetos").val()).load();
        });

        $(document).ready(function() {
            $.getJSON('controller/usuario/responsaveis.php',function(responsaveis){
                for(i in responsaveis){
                    $("#id_usuario").append($('<option>', {value: responsaveis[i].id,text: responsaveis[i].nome}));
                }
                
                $.getJSON('controller/projeto/lista_por_usuario.php?id_usuario='+$("#id_usuario").val(),function(projetos){
                    for(i in projetos){
                        $("#projetos").append($('<option>', {value: projetos[i].id,text: projetos[i].nome}));
                    }
                    setProjeto($("#projetos").val());
                    table = $('table').DataTable({
                        order: [[ 0, "desc" ]],
                        ajax: {url: "controller/publicacao/lista.php?id_projeto="+$("#projetos").val(), dataSrc: ""},
                        columns: [
                            { data: "id" },
                            {"render": function (data, type, full, meta){ 
                                    niveis = full.caminho.split("/");
                                    return niveis[niveis.length-1];
                            }},
                            { data: "data_cadastro" },
                            {"render": function (data, type, full, meta){ 
                                niveis = full.caminho.split("/");
                                url = "upload/"+niveis[niveis.length-3]+"/"+niveis[niveis.length-2]+"/"+niveis[niveis.length-1];
                                
                                extensao = niveis[niveis.length-1].split('.').pop();
                                botao = "";

                                switch(extensao) {
                                    case 'docx':
                                    case 'doc':
                                        botao = "<button class='btn btn-primary data-toggle view-office' data-href='"+url+"' arquivo-nome='"+niveis[niveis.length-1]+"' title='Arquivo Word'><i class='fas fa-file-word'></i></button>";
                                        break;
                                    case 'pptx':
                                    case 'ppt':
                                    case 'pps':
                                        botao = "<button class='btn btn-warning data-toggle view-office' data-href='"+url+"' arquivo-nome='"+niveis[niveis.length-1]+"' title='Apresentação Power Point'><i class='fas fa-file-powerpoint'></i></button>";
                                        break;
                                    case 'xlsx':
                                    case 'xls':
                                        botao = "<button class='btn btn-success data-toggle view-office' data-href='"+url+"' arquivo-nome='"+niveis[niveis.length-1]+"' title='Planilha'><i class='fas fa-file-excel'></i></button>";
                                        break;
                                    default:
                                        botao = "<a class='btn btn-danger data-toggle' target='_blank' href='"+url+"' title='Arquivo PDF'><i class='far fa-file-pdf'></i></a>"
                                }

                                return botao;
                            }}
                            <?php 
                                if ($_SESSION["login_tipo"] != 'P') {
                                    echo ',{"render": function (data, type, full, meta)';
                                    echo '{ return "<a class=\'btn btn-danger remover\' id=\'" + full.id + "\'><i class=\'far fa-trash-alt\'></i></a>"; }';
                                    echo '}';
                                }
                            ?>
                        ],
                        bAutoWidth: false,
                        info: false,
                        lengthChange: false,
                        language: {search: 'Busca:', searchPlaceholder: "", emptyTable: "Nenhum registro encontrado.", paginate: {"next": "Próxima", "previous": "Anterior"}}
                    });
                });
            });

            $('body [data-toggle="tooltip"]').tooltip();
        });

        $('body').on('click', '.editar', function(){
            id = $(this).attr("id");
            $.get('controller/publicacao/formulario.php',{id: id},function(data){
                publicacao = JSON.parse(data)[0];
                $("#id").val(publicacao.id);
                $("#nome").val(publicacao.nome);
                listaArquivos(publicacao.arquivos);
                $("#cadastro-tab").click();
                $("#div-upload").html("").hide();
            });            
        });

        $('body').on('click', '.remover', function(){
            if(tipo_usuario != 'P'){
                id = $(this).attr("id");
                modal({title: "Deseja remover esse arquivo?", confirmTitle: "Sim", cancelTitle: "Não"}, function(){
                    $.ajax({url: "controller/publicacao/remover.php?id="+id, type: "post", dataType: "html", contentType: false, cache: false, processData: false,
                        success: function (result) {
                            $("#alerta-sucesso").html(result).show();
                            $("#btn-novo").click();
                            $('table').DataTable().ajax.reload();
                            $('.border-danger').removeClass("border-danger");
                        },
                        error: function (result) {
                            console.log(result);
                        }
                    }); 
                });                       
            }
            else{
                modal({title: "Você não pode remover esse produto devido as permissões de seu usuário.", confirmTitle: "Entendi, sou Pesquisador.", cancel: false});
            }
        });

        $('form').submit(function (evt) {
            evt.preventDefault();
            
            if(tools.validaCampos()){
                $.ajax({
                    url: "controller/publicacao/salvar.php",
                    type: "post",
                    dataType: "html",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (result) {
                        $("#alerta-sucesso").html(result).show();
                        $(".border-danger").removeClass("border-danger");
                        $("table").DataTable().ajax.reload();
                        $('#fileLabel').text("Escolha os arquivos...");
                        $("#div-modal-add-pub").modal("hide");
                    },
                    error: function (result) {
                        console.log(result);
                    }
                });
            }
        });

        $('body').on('click', '.view-office', function(){
            var alturaIframe = $('body').height();
            // console.log("calc(" + alturaIframe + " - " + (alturaIframe * 0.2) + ")");
            // $("#div-frame-office .modal-body").css('border', '2px solid red').css('height', "calc(" + alturaIframe + " - " + (alturaIframe * 0.2) + ")");
            $("#titulo-arquivo").html('Visualizando o arquivo: ' + $(this).attr('arquivo-nome'));
            $("#iframe-office").attr("src", "https://view.officeapps.live.com/op/embed.aspx?src=http://cgpdi.org.br/cgpdi_admin/" + $(this).attr("data-href"));
            $("#div-frame-office").modal('show');
        });

        isOffice = function(arquivo){
            extensoes = [".doc", ".docx", ".ppt", ".pptx", ".ppsx", ".xls", ".xmlx", ".pps"];
            for(i in extensoes){
                if(arquivo.indexOf(extensoes[i]) !== -1){
                    return true
                }
            }
            return false;
        }

        listaArquivos = function(arquivos){
            paths = arquivos.split(";");
            
            $("#div-upload").hide();
            for(i in paths){
                nome = niveis[niveis.length-1];
                console.log(nome);
                $("#div-files").append("<a class='btn btn-warning data-toggle' target='_blank' href='"+url+"' title='"+nome+"'><i class='far fa-file-pdf'></i></a>");
            }
            $("#div-files").show();
        }

        $("#files").change(function() {
            $("#fileLabel").html("");            
            
            for (i = 0; i < this.files.length; i++){
                console.log(this.files);
                $("#fileLabel").append(this.files[i].name+";");
            }
            return true;
        });
    </script>
</body>
</html>