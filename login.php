<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>CGPDI Admin</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/signin.css" rel="stylesheet">
    </head>
    <body class="text-center">
        <form class="form-signin" method="post" action="controller/login/logar.php">
            <img class="mb-4" src="img/logo.png" alt="" width="111" height="84">
            <h1 class="h3 mb-3 font-weight-normal">CGPDI Admin</h1>

            <label for="inputEmail" class="sr-only">E-mail</label>
            <input type="email" name="txtEmail" id="inputEmail" class="form-control" placeholder="E-mail" required="" autofocus="">

            <label for="inputPassword" class="sr-only">Senha</label>
            <input type="password" name="txtSenha" id="inputPassword" class="form-control" placeholder="Senha" required="">

            <button class="btn btn-lg btn-success btn-block" type="submit">Entrar</button>
            <p class="mt-5 mb-3 text-muted">© 2018</p>
        </form>
    </body>
</html>
