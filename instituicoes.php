<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CGPDI Admin</title>

    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/index.css">
    <link rel="stylesheet" href="css/datatables.min.css">

</head>
<body>
    <?php include_once "cabecalho.php";?>

    <main role="main" class="container">
        <h2>Instituições</h2>
        <hr>
        <ul class="nav nav-tabs" id="cabecalho.phpTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="cadastro-tab" data-toggle="tab" href="#cadastro" role="tab" aria-controls="cadastro" aria-selected="true">Cadastro</a>
            </li>
                
            <li class="nav-item">
                <a class="nav-link" id="lista-tab" data-toggle="tab" href="#lista" role="tab" aria-controls="lista" aria-selected="false">Lista</a>
            </li>
        </ul>
        
        <div class="tab-content" id="cabecalho.phpTabConteudo">
            <div class="tab-pane fade show active" id="cadastro" role="tabpanel" aria-labelledby="cadastro-tab">
                <form method="post" enctype="multipart/form-data" onreset="javascrit:$('#id').val('');">
                    <input type="hidden" class="form-control" id="id" name="id">
                    <div class="form-group col-sm clearfix">
                        <button type="submit" class="btn btn-success float-right" id="btn-cadastrar">
                            <i class="fas fa-save"></i>
                        </button>
                        
                        <button class="btn btn-secondary float-right" id="btn-novo">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-5">
                            <label for="nome">Nome</label>
                            <input type="text" class="form-control required" id="nome" name="nome" maxlength="60">
                        </div>
                        
                        <div class="form-group col-md-7" id="div-upload">
                            <label for="upload">Upload Imagem</label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input required" id="files" name="files[]" accept=".png, .jpg, .jpeg">
                                    <label class="custom-file-label" for="files" id="fileLabel">Escolha os caminho...</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-7 hidden" id="div-files"></div>
                    </div>
                </form>
            </div>
            <div class="tab-pane fade" id="lista" role="tabpanel" aria-labelledby="lista-tab">
                <div class="alert alert-danger hidden" role="alert" id="alerta-erro"></div>
                <div class="alert alert-success hidden" role="alert" id="alerta-sucesso"></div>
                <hr>
                <div class="row">
                    <div class="col-sm">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="listaClientes">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Nome</th>
                                        <th>Caminho</th>
                                    </tr>
                                </thead>
                                <tbody/>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <?php include_once "modal/Modal.php";?>

    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/datatables.min.js"></script>
    <script src="js/jquery.mask.min.js"></script>
    <script src="js/cgpdi.js"></script>
    <script src="js/modal.js"></script>

    <script>
        $('body .data-toggle').tooltip();

        var div_upload= $("#div-upload").html();

        $("#btn-novo").click(function(){
            $('input[type="text"], input[type="date"], input[type="hidden"], input[type="file"]').val('');
            //$("#fileLabel").html('');
            $("#div-upload").html(div_upload).show();
            $("#div-files").html("").hide();
        });
        
        $(document).ready(function() {
            $('table').DataTable({
                order: [[ 0, "desc" ]],
                ajax: {
                    url: "controller/instituicao/lista.php",
                    dataSrc: ""
                },
                columns: [
                    { data: "id" },
                    { data: "nome" },
                    {"render": function (data, type, full, meta){
                        niveis = full.caminho.split("/");
                        return niveis[niveis.length-1];
                    }},
                    {"render": function (data, type, full, meta)
                        { return '<a class="btn btn-warning editar" id="' + full.id + '"><i class="far fa-edit"></i></a>'; }
                    },
                    {"render": function (data, type, full, meta)
                        { return '<a class="btn btn-danger remover" id="' + full.id + '"><i class="far fa-trash-alt"></i></a>'; }
                    }
                ],
                bAutoWidth: false,
                info: false,
                lengthChange: false,
                language: {
                    search: 'Busca:',
                    searchPlaceholder: "",
                    emptyTable: "Nenhum registro encontrado.",
                    paginate: {
                        "next": "Próxima",
                        "previous": "Anterior"
                    }
                }
            });

            $('body [data-toggle="tooltip"]').tooltip();
        });

        $('body').on('click', '.editar', function(){
            id = $(this).attr("id");
            $.get('controller/instituicao/formulario.php',{id: id},function(data){
                instituicao = JSON.parse(data)[0];
                $("#id").val(instituicao.id);
                $("#nome").val(instituicao.nome);
                listaArquivos(instituicao.caminho);
                $("#cadastro-tab").click();
                $("#div-upload").html("").hide();
            });            
        });

        $('body').on('click', '.remover', function(){
            id = $(this).attr("id");
            modal({title: "Deseja realmente excluir essa Licitação?", confirmTitle: "Sim", cancelTitle: "Não"}, function(){
                $.ajax({url: "controller/instituicao/remover.php?id="+id, type: "post", dataType: "html", contentType: false, cache: false, processData: false, success: function (result) {
                    $("#alerta-sucesso").html(result).show();
                    $("#btn-novo").click();
                    $('table').DataTable().ajax.reload();
                    $('.border-danger').removeClass("border-danger");
                },
                error: function (result) {
                    console.log(result);
                }}); 
            });
                        
        });

        $('form').submit(function (evt) {
            evt.preventDefault();
            
            if(tools.validaCampos()){
                $.ajax({
                    url: "controller/instituicao/salvar.php",
                    type: "post",
                    dataType: "html",
                    data: new FormData(this),
                    contentType: false,
                    cache: false,
                    processData: false,
                    success: function (result) {
                        $("#alerta-sucesso").html(result).show();
                        $("#btn-novo").click();
                        $('.border-danger').removeClass("border-danger");
                    },
                    error: function (result) {
                        console.log(result);
                    }
                }).done(function(){
                    $('table').DataTable().ajax.reload();
                    $("#lista-tab").click();
                });
            }
        });

        listaArquivos = function(caminho){
            console.log(caminho)
            $("#div-upload").hide();
            niveis = caminho.split("/");
            url = "upload/"+niveis[niveis.length-2]+"/"+niveis[niveis.length-1];
            nome = niveis[niveis.length-1];
            $("#div-files").html("<img src='"+url+"' />");
            //$("#div-files").html("<label for='files'>Imagem</label><div><a class='btn btn-warning data-toggle' target='_blank' href='"+url+"' title='"+nome+"'><i class='far fa-file-image'></i></a></div>");
            
            $("#div-files").show();
        }

        $("#files").change(function() {
            var arquivo = this.files[0];
            var extensaoArquivo = arquivo.type;
            var extensoes= ["image/jpeg","image/png","image/jpg"];

            if(!((extensaoArquivo==extensoes[0]) || (extensaoArquivo==extensoes[1]) || (extensaoArquivo==extensoes[2]))){
                modal({title: "Escolha um arquivo valido (.jpg, .jpeg, .png)", cancel: false}, function(){
                    $("#fileLabel").html('Escolha uma imagem...');
                });
                return false;
            }else{
                $("#fileLabel").html(arquivo.name);
            }
        });
    </script>
</body>
</html>