<?php
    class Upload{
        
        private $pasta;
        
        public function __construct($parentFolder){
            $this->pasta = dirname(__DIR__)."/upload/".$parentFolder."/";
            
            if (!file_exists($this->pasta)) {
                mkdir($this->pasta, 0755, true);
            }
        }

        //Arquivo vindo do $_POST
        public function fromFile($arq) {
            try{
                $f = $this->pasta.$arq['name'];
                if (move_uploaded_file ($arq['tmp_name'], $f)) {
                    chmod($f, 0644);
                    return $f;
                }else{
                    echo "Erro ao enviar arquivo ".$arq['name']." <br />";
                    return "";
                }
            }catch(Exception $ex){
                echo $ex->getMessage()+" <br/>";
            }
        }

        //Arquivos vindo do $_POST
        public function fromFiles($arqs) {
            
            $paths=array();

            $file_count = count($arqs['name']);
            for ($i=0; $i<$file_count; $i++) {
                try{
                    $f = $this->pasta.$arqs['name'][$i];
                    if (move_uploaded_file ($arqs['tmp_name'][$i], $f)) {
                        chmod($f, 0644);
                        array_push($paths, $f);
                    }else{
                        echo "Erro ao enviar arquivo ".$arqs['name'][$i]." <br />";
                    }
                }catch(Exception $ex){
                    echo $ex->getMessage()+" <br/>";
    
                }
            }
            return join(";", $paths);
        }

        public function removeFile($file) { 
            try{ 
                unlink($file); 
            }catch(Exception $ex){
                echo "Erro ao remover o arquivo.".$this->pasta;
            }
            
        }

        public function remove() {
            try{
                if(!$this->removePasta($this->pasta))
                    echo "Erro ao remover pasta de arquivos ".$this->pasta;
            }catch(Exception $ex){
                echo "Exceção gerada ao remover pasta de arquivos ".$this->pasta;
            }
        }

        private function removePasta($dir) { 
            $files = array_diff(scandir($dir), array('.','..')); 
            foreach ($files as $file) { 
              (is_dir("$dir/$file")) ? removePasta("$dir/$file") : unlink("$dir/$file"); 
            } 
            return rmdir($dir); 
        }
    }
?>